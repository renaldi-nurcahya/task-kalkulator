// Online C compiler to run C program online
#include <stdio.h>

int main() {
    // Write C code here
    int b, hasil, menu;
    
    printf("====================================\n");
    printf("Program Kalkulator Sederhana\n");
    printf("====================================\n\n");
    
    mulai:
    printf("Masukan nilai pertama : ");
    scanf("%d", &hasil);
    printf("------------------------------- \n");
    
    pilih:
    printf("Menu :\n");
    printf("\t1. Penjumlahan\n");
    printf("\t2. Pengurangan\n");
    printf("\t3. Perkalian\n");
    printf("\t4. Pembagian\n");
    printf("\t5. Modulus\n");
    printf("\t6. Get Results\n\n");
    printf("Pilih menu : ");
    scanf("%d",&menu);
    printf("-------------------------------\n\n");
    
    switch (menu){
      case 1:
        printf("Masukan nilai lagi : ");
        scanf("%d", &b);
        hasil = hasil + b;
        goto pilih;
        break;
    case 2:
        printf("Masukan nilai lagi : ");
        scanf("%d", &b);
        hasil = hasil - b;
        goto pilih;
        break;
    case 3:
        printf("Masukan nilai lagi : ");
        scanf("%d", &b);
        hasil = hasil * b;
        goto pilih;
        break;
    case 4:
        printf("Masukan nilai lagi : ");
        scanf("%d", &b);
        hasil = hasil / b;
        goto pilih;
        break;
    case 5:
        printf("Masukan nilai lagi : ");
        scanf("%d", &b);
        hasil = hasil % b;
        goto pilih;
        break;
    case 6:
        printf("Hasil:\t\t\t   %d\n", hasil);
        break;
    default:
        printf("Anda salah memilih menu.");
        break;
    }
    return 0;
}